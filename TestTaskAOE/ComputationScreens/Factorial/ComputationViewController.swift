import UIKit

struct ComputationViewConfiguration {
    let title: String
    let successPhrase: String
}

protocol ComputationView {
    func computationResult(string: String)
}

class ComputationViewController: UIViewController {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var resultLabel: UILabel!

    @IBOutlet weak var inputField: UITextField!

    var configuration: ComputationViewConfiguration?
    var output: ((String) -> ())?

    convenience init(with configuration: ComputationViewConfiguration) {
        self.init()
        self.configuration = configuration
    }

    override func viewDidLoad() {
        super.viewDidLoad()
         self.titleLabel.text = configuration?.title
    }

    @IBAction func sendData(_ sender: Any) {
        guard let text = inputField.text else {
            return
        }

        self.output?(text)
    }
}

extension ComputationViewController: ComputationView {
    func computationResult(string: String) {
        guard let configuratoinsuccessPhrase = self.configuration?.successPhrase else {
            return
        }

        resultLabel.text = "\(configuratoinsuccessPhrase): \(string)"
    }
}
