import Foundation

protocol FactorialDigitsSummatorFactory {
    func getInstance() -> FactorialDigitsSummator
}

protocol PairCheckerFactory {
    func getInstance() -> SubarrayFinder
}

protocol PalindromCheckerFactory {
    func getInstance() -> PalindromFinder
}


class ConcretePalindromComputationFactory: PalindromCheckerFactory {
    func getInstance() -> PalindromFinder {
        return PalindromFinder()
    }
}

class ConcretePairCheckerComputationFactory: PairCheckerFactory {
    func getInstance() -> SubarrayFinder {
        return SubarrayFinder()
    }
}

class ConcreteFactorialDigitsSummerFactory: FactorialDigitsSummatorFactory {
    func getInstance() -> FactorialDigitsSummator {
        return FactorialDigitsSummator()
    }
}


