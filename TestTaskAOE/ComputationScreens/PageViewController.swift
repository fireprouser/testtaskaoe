import UIKit

class PageViewController: UIPageViewController {
    var controllers: [UIViewController]?

    convenience init(with controllers: [UIViewController]) {
        self.init()
        self.controllers = controllers
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        let factorial = ComputationViewController(with: ComputationViewConfiguration(title: "", successPhrase: ""))
         let palindrom = ComputationViewController(with: ComputationViewConfiguration(title: "", successPhrase: ""))
        self.setViewControllers([factorial, palindrom], direction: .forward, animated: true, completion: nil)
    }
}
