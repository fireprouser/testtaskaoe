import Foundation

class FactorialComputationPresenter {
    let view: ComputationView
    let factory: FactorialDigitsSummatorFactory

    init(with view: ComputationView, factory: FactorialDigitsSummatorFactory) {
        self.view = view
        self.factory = factory
    }

    func compute(string: String) {
        guard let number = Int(string) else {
            view.computationResult(string: "Invalid input")
            return
        }

        let summator = factory.getInstance()
        let result = summator.sum(number)
        view.computationResult(string: "\(result)")
    }
}
