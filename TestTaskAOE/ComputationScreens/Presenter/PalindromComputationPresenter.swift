import Foundation

class PalindromComputationPresenter {
    let view: ComputationView
    let factory: PalindromCheckerFactory

    init(with view: ComputationView, factory: PalindromCheckerFactory) {
        self.view = view
        self.factory = factory
    }
    
    func compute(string: String) {
        guard Int(string) != nil else {
            view.computationResult(string: "Invalid input")
            return
        }

        let finder = factory.getInstance()
        let result = finder.check(string)
        view.computationResult(string: "\(result)")
    }
}
