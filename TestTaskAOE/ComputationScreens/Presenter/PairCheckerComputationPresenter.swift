import Foundation

class PairCheckerComputationPresenter {
    let view: ComputationView
    let factory: PairCheckerFactory
    
    init(with view: ComputationView, factory: PairCheckerFactory) {
        self.view = view
        self.factory = factory
    }
    
    func compute(string: String) {
        guard let parsedDubles = self.parse(string: string) else {
            view.computationResult(string: "Invalid input")
            return
        }
        
        let finder = self.factory.getInstance()
        let result = finder.find(parsedDubles)
        view.computationResult(string: "\(result)")
    }
    
    private func parse(string: String) -> [(Int, Int)]? {
        let pairs = string.components(separatedBy: ",")
        var result: [(Int, Int)] = []
        
        for pair in pairs {
            let components = pair.components(separatedBy: " ")
            guard components.count == 2, let first = Int(components[0]), let second = Int(components[1]) else {
                return nil
            }
            
            result.append((first, second))
        }
        
        return result
    }
}
