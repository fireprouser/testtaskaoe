import UIKit

class FlowCoordinator: Coordinator {
    var childCoordinators: [Coordinator]
    var navigationController: UINavigationController

    init(with navigationController: UINavigationController) {
        self.navigationController = navigationController
        self.childCoordinators = []
    }

    func start() {
        let loginViewController = LoginViewController { (login, password) in
            self.showComputationScreen()
        }

        navigationController.pushViewController(loginViewController, animated: true)
    }

    func showComputationScreen() {
        let tabVC = UITabBarController()

        let palindromVC = ComputationViewController(with: ComputationViewConfiguration(title: "Enter the number to check it is palindrom:", successPhrase: "Is it palindrom:"))
        palindromVC.tabBarItem = UITabBarItem(title: "Palindrom", image: nil, tag: 0)
        let palindromPresenter = PalindromComputationPresenter(with: palindromVC, factory: ConcretePalindromComputationFactory())
        palindromVC.output = { string in
            palindromPresenter.compute(string: string)
        }

        let factorialVC = ComputationViewController(with: ComputationViewConfiguration(title: "Enter the number", successPhrase: "Factorial:"))
        factorialVC.tabBarItem = UITabBarItem(title: "Factorial", image: nil, tag: 1)
        let factorialPresenter = FactorialComputationPresenter(with: factorialVC, factory: ConcreteFactorialDigitsSummerFactory())
        factorialVC.output = { string in
            factorialPresenter.compute(string: string)
        }

        let pairCheckerVC = ComputationViewController(with: ComputationViewConfiguration(title: "Enter pairs in format: 1 2, 2 3,4 5", successPhrase: "Pairs:"))
        pairCheckerVC.tabBarItem = UITabBarItem(title: "Pair checker", image: nil, tag: 2)
        let paircheckerPresenter = PairCheckerComputationPresenter(with: pairCheckerVC, factory: ConcretePairCheckerComputationFactory())
        pairCheckerVC.output = { string in
            paircheckerPresenter.compute(string: string)
        }

        tabVC.setViewControllers([palindromVC, pairCheckerVC, factorialVC], animated: false)
        navigationController.pushViewController(tabVC, animated: true)
    }
}

