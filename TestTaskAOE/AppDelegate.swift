//
//  AppDelegate.swift
//  TestTaskAOE
//
//  Created by Dmytro on 11/9/19.
//  Copyright © 2019 ReliableSoftwareImp. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {

        let navigationController = UINavigationController()

        let flowCoordinator = FlowCoordinator(with: navigationController)
        flowCoordinator.start()

        window = UIWindow(frame: UIScreen.main.bounds)
        window?.rootViewController = navigationController
        window?.makeKeyAndVisible()


        return true
    }
}

