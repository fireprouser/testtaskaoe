import UIKit

class LoginViewController: UIViewController {
    @IBOutlet weak var sendButton: UIButton!

    @IBOutlet weak var loginField: UITextField!
    @IBOutlet weak var passwordField: UITextField!

    var login: ((String, String) -> ())?

    convenience init(login: @escaping ((String, String) -> ())) {
        self.init()
        self.login = login
    }

    @IBAction func sendButtonClicked(_ sender: Any) {
        guard let login = loginField.text, let password = passwordField.text else {
            return
        }

        self.login?(login, password)
    }

}
