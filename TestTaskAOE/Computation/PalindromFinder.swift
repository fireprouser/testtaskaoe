import Foundation

class PalindromFinder {
    func check(_ string: String) -> Bool {
        
        guard !string.isEmpty else {
            return false
        }
        
        let array = Array(string)
        let stringLength = array.count
        let isStringLenghtOdd = stringLength % 2 == 0
        
        var leftBoarder = 0
        var rightBoarder = stringLength - 1
        
        var lengthCondition:(Int, Int) -> Bool
        
        if isStringLenghtOdd {
            lengthCondition = { leftBorarder, rightBoarder in
                return leftBoarder - rightBoarder == 1
            }
        } else {
            let midPoint = stringLength / 2
            
            lengthCondition = { leftBorarder, rightBoarder in
                return leftBoarder == midPoint && rightBoarder == midPoint
            }
        }
        
        
        while (array[leftBoarder] == array[rightBoarder])
        {
            if lengthCondition(leftBoarder, rightBoarder) {
                return true
            }
            
            leftBoarder += 1
            rightBoarder -= 1
        }
        
        
        
        return false
    }
}
