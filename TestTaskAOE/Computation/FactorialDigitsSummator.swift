import Foundation

class FactorialDigitsSummator {
    private var numbersArray: [Int]

    init() {
        self.numbersArray = [1]
    }

    private func multiply(by number: Int) {
        var rest = 0

        for i in 0..<numbersArray.count {
            let product = self.numbersArray[i] * number + rest
            self.numbersArray[i] = product % 10
            rest = product / 10
        }

        while rest != 0 {
            self.numbersArray.append(rest % 10)
            rest /= 10
        }

    }

    func sum(_ orderNumber: Int) -> Int {
        guard orderNumber > 2 else {
            return orderNumber
        }

        for number in 2...orderNumber {
            self.multiply(by: number)
        }

        return self.numbersArray.reduce(0) {$0 + $1}
    }
}
