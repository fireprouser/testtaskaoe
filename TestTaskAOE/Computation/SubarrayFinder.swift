import Foundation

class SubarrayFinder {
    func find(_ array: [(Int, Int)]) -> [(Int, Int)] {
        var startPosition = -1
        
        var subsequence: [(Int, Int)] = []
        
        for i in 0..<array.count - 1 {
            let firstDuple = array[i]
            let secondDuple = array[i + 1]
            
            if firstDuple.0 < secondDuple.0 && firstDuple.1 > secondDuple.1 {
                if (startPosition == -1) {
                    startPosition = i
                }
            } else {
                if (startPosition == -1) {
                    continue
                }
                subsequence.append((startPosition, i))
                startPosition = -1
            }
        }
        
        let longestSequence = subsequence.sorted { (first, second) -> Bool in
            return first.1 - first.0 > second.1 - first.0
        }.first
        
        guard let longest = longestSequence else {
            return []
        }
        
        return Array(array[longest.0...longest.1])
    }
}
