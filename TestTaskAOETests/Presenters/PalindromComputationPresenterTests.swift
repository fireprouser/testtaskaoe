import XCTest

@testable import TestTaskAOE

class PalindromComputationPresenterTests : XCTestCase {
    func test_wrongInput() {
        let spyView = ComputationViewSpy()
        let factorialPresenter = PalindromComputationPresenter(with: spyView, factory: ConcretePalindromComputationFactory())
        factorialPresenter.compute(string: "1gsdfg")
        XCTAssertEqual(spyView.lastResult, "Invalid input")
    }

    func test_outputInput() {
       let spyView = ComputationViewSpy()
       let factorialPresenter = PalindromComputationPresenter(with: spyView, factory: ConcretePalindromComputationFactory())
       factorialPresenter.compute(string: "151")
       XCTAssertEqual(spyView.lastResult, "true")
    }
}
