import XCTest

@testable import TestTaskAOE

class ComputationViewSpy: ComputationView {
    var computationResultCount: Int = 0
    var lastResult: String = ""

    func computationResult(string: String) {
        lastResult = string
        computationResultCount += 1
    }
}
