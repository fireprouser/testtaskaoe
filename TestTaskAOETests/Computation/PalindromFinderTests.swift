import XCTest

@testable import TestTaskAOE

class PalindromFinderTests: XCTestCase {

    func test_palindromOddNegative() {
        let palindromFinder = PalindromFinder()
        XCTAssertFalse(palindromFinder.check("abcd"))
    }

    func test_palindromNotOddNegative() {
        let palindromFinder = PalindromFinder()
        XCTAssertFalse(palindromFinder.check("abbcd"))
    }

    func test_palindromOdd() {
        let palindromFinder = PalindromFinder()
        XCTAssertTrue(palindromFinder.check("abba"))
    }

    func test_palindromNotOdd() {
        let palindromFinder = PalindromFinder()
        XCTAssertTrue(palindromFinder.check("abbba"))
    }

    func test_palindromEmptyString() {
        let palindromFinder = PalindromFinder()
        XCTAssertFalse(palindromFinder.check(""))
    }
}
