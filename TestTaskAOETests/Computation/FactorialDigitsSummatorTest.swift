import XCTest

@testable import TestTaskAOE

class FactorialDigitsSummatorTest: XCTestCase {
    func test_sumZero() {
        let sut = FactorialDigitsSummator()
        XCTAssertEqual(sut.sum(0), 0)
    }

    func test_sumOne() {
        let sut = FactorialDigitsSummator()
        XCTAssertEqual(sut.sum(1), 1)
    }

    func test_factorial4() {
        let sut = FactorialDigitsSummator()
        XCTAssertEqual(sut.sum(4), 6)
    }

    func test_bigFactorialTwo() {
        let sut = FactorialDigitsSummator()
        XCTAssertEqual(sut.sum(54), 261)
    }

    func test_bigFactorial() {
        let sut = FactorialDigitsSummator()
        XCTAssertEqual(sut.sum(55), 279)
    }

    func test_100Factorial() {
        let sut = FactorialDigitsSummator()
        XCTAssertEqual(sut.sum(100), 648)
    }
}
