import XCTest

@testable import TestTaskAOE

class PairCheckerComputationPresenterTests: XCTestCase {
    func test_wrongInput() {
        let spyView = ComputationViewSpy()
        let factorialPresenter = PairCheckerComputationPresenter(with: spyView, factory: ConcretePairCheckerComputationFactory())
        factorialPresenter.compute(string: "1gsdfg")
        XCTAssertEqual(spyView.lastResult, "Invalid input")
    }

    func test_outputInput() {
       let spyView = ComputationViewSpy()
       let factorialPresenter = PairCheckerComputationPresenter(with: spyView, factory: ConcretePairCheckerComputationFactory())
       factorialPresenter.compute(string: "1 4,2 5,7 3,4 6")
       XCTAssertEqual(spyView.lastResult, "[(2, 5), (7, 3)]")
    }
}
