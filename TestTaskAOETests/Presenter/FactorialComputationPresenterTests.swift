import XCTest

@testable import TestTaskAOE

class FactorialComputationPresenterTests: XCTestCase {
    func test_wrongInput() {
        let spyView = ComputationViewSpy()
        let factorialPresenter = FactorialComputationPresenter(with: spyView, factory: ConcreteFactorialDigitsSummerFactory())
        factorialPresenter.compute(string: "fsd")
        XCTAssertEqual(spyView.lastResult, "Invalid input")
    }

    func test_outputInput() {
       let spyView = ComputationViewSpy()
       let factorialPresenter = FactorialComputationPresenter(with: spyView, factory: ConcreteFactorialDigitsSummerFactory())
       factorialPresenter.compute(string: "50")
       XCTAssertEqual(spyView.lastResult, "216")
    }
}
