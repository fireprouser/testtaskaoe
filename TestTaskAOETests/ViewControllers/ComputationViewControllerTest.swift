import XCTest

@testable import TestTaskAOE

class ComputationViewControllerTest: XCTestCase {

    func test_viewDidLoad_checkIsConfigurationApplied() {
        let config = ComputationViewConfiguration(title: "Title", successPhrase: "succ")
        let sut = ComputationViewController(with: config)
        sut.loadViewIfNeeded()
        let result = "141"
        sut.computationResult(string: result)
        XCTAssertEqual(sut.titleLabel.text, config.title)
        XCTAssertEqual(sut.resultLabel.text, "\(config.successPhrase): \(result)")
    }

    func test_viewDidLoad_sendDataCheck() {
        let config = ComputationViewConfiguration(title: "Title", successPhrase: "succ")
        let sut = ComputationViewController(with: config)
        sut.loadViewIfNeeded()
        let computSting = "comput"
        sut.inputField.text = computSting
        sut.output = { string in
            XCTAssertEqual(string, computSting)
        }
        sut.sendData(UIView())
    }

}
