import XCTest

@testable import TestTaskAOE

class LoginViewControllerTest: XCTestCase {

    func test_viewDidLoad_sendDataFromUI() {
        let testLogin = "login"
        let testPassword = "password"

        let sut = LoginViewController { (login, password) in
            XCTAssertEqual(login, testLogin)
            XCTAssertEqual(password, testPassword)
        }

        sut.loadViewIfNeeded()
        sut.loginField.text = testLogin
        sut.passwordField.text = testPassword
        sut.sendButtonClicked(UIView())
    }

}
