import XCTest
@testable import TestTaskAOE

class SubarrayFinderTests: XCTestCase {

    func test_findLongestSequence() {
        let subarrayFinder = SubarrayFinder()
        let array = [(1,4), (2,5), (7, 3), (4, 6),(7, 7)]
        let result = subarrayFinder.find(array)
        print(result)
        XCTAssert(self.compare(lhs: result, rhs: [(2,5), (7, 3)]))
    }

    func test_findLongestSequenceThreeElements() {
        let subarrayFinder = SubarrayFinder()
        let array = [(1,4), (2,5), (7, 3), (9, 1), (4, 6), (7, 7)]
        let result = subarrayFinder.find(array)
        print(result)
        XCTAssert(self.compare(lhs: result, rhs: [(2,5), (7, 3), (9, 1)]))
    }

    func test_resultWithoutSequnce() {
        let subarrayFinder = SubarrayFinder()
        let array = [(1,4)]
        let result = subarrayFinder.find(array)
        XCTAssert(result.isEmpty)
    }

    func compare(lhs: [(Int, Int)], rhs: [(Int, Int)]) -> Bool{
        guard lhs.count == rhs.count else {
            return false
        }

        for i in 0..<lhs.count {
            if lhs[i] != rhs[i] {
                return false
            }
        }

        return true
    }


}
